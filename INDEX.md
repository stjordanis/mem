# MEM

See [https://github.com/FDOS/mem](https://github.com/FDOS/mem)


## Contributions

NLS specific corrections, updates and submissions should not be 
directly to submitted this project. NLS is maintained at the [FD-NLS](https://github.com/shidel/fd-nls)
project on GitHub. If the project is still actively maintained by it's
developer, it may be beneficial to also submit changes to them directly.

## MEM.LSM

<table>
<tr><td>title</td><td>MEM</td></tr>
<tr><td>version</td><td>1.11a</td></tr>
<tr><td>entered&nbsp;date</td><td>2007-11-30</td></tr>
<tr><td>description</td><td>Displays the amount of used and free memory in your system</td></tr>
<tr><td>keywords</td><td>Memory, mem, xms, ems, umb, hma</td></tr>
<tr><td>author</td><td>Joe Cosentino &lt;jayc17@mediaone.net&gt;</td></tr>
<tr><td>maintained&nbsp;by</td><td>Bart Oldeman &lt;bartoldeman@users.sourceforge.net&gt;</td></tr>
<tr><td>primary&nbsp;site</td><td>https://gitlab.com/FDOS/base/mem</td></tr>
<tr><td>original&nbsp;site</td><td>http://www.dosemu.org/~bart</td></tr>
<tr><td>platforms</td><td>FreeDOS</td></tr>
<tr><td>copying&nbsp;policy</td><td>[GNU General Public License, Version 2](LICENSE)</td></tr>
<tr><td>wiki&nbsp;site</td><td>http://wiki.freedos.org/wiki/index.php/Mem</td></tr>
</table>
